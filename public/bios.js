"use strict";
class Bios {
	constructor() {
		new Audio("res/audio/drivestartup.ogg").play()

		this.messages = [
			//[Delay before showing, function or string], ...
			[1500, this.swap_to_bios],
			[100, "Processor   : Intel Pentium II @ 233MHz with MMX"],
			[50, "\nMemory Test : "],
			[0, this.print_memTest],
			[1620, "\nMain Memory : 128M SRDAM @ 100MHz"],
			[50, "\n\nDetecting IDE Primary Master  ... "],
			[100, "MAXTOR 6LA48J2\nDetecting IDE Primary Slave   ... "],
			[500, "None\nDetecting IDE Secondary Master... "],
			[500, "ATAPI CDROM\nDetecting IDE Secondary Slave ... "],
			[500, "None"],
			[500, "\n\n\nAttempting to boot Floppy Master. "],
			[0, this.floppy_throb],
			[1500, this.floppy_throb_stop],
			[50, "\nAttempting to boot IDE Primary Master. "],
			[50, "Booting..."],
			[50, this.clear_term],
			[1500, this.graphical_boot],
		]
		this.message_index = 0
		this.terminal_cursor_blink = 0

		$("body").addClass("hide-cursor")
		$("#bios-vga").show()
		this.blink_interval = setInterval(this.blink_cursor, 270)
	}

	run() {
		const message_info = this.messages[this.message_index++]
		if (message_info === undefined)
			return;

		const delay = message_info[0]
		const message = message_info[1]
		const dom_bios_status = $("#bios-status")

		if (typeof message === 'function') {
			setTimeout(() => {
				message.apply(this)
				this.run()
			}, delay)
		}
		else {
			setTimeout(() => {
				dom_bios_status.append(message)
				this.run()
			}, delay)
		}
	}

	blink_cursor() {
		if (this.terminal_cursor_blink === 0) {
			this.terminal_cursor_blink = 1
			$(".terminal-cursor").text(' ')
		}
		else {
			this.terminal_cursor_blink = 0
			$(".terminal-cursor").text('_')
		}
	}

	swap_to_bios() {
		$("#bios-vga").hide()
		$("#bios").show()
	}

	print_memTest() {
		const dom_status = $("#bios-status")
		dom_status.append('<span id="bios-status-mem"></span> OK')
		const dom_mem = $("#bios-status-mem")
		const update_mem = i => {
			//Source: https://stackoverflow.com/a/2901298
			dom_mem.text(i.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
			if (i <= 130048)
				setTimeout(() => update_mem(i + 1024), 10)
			else {
				dom_mem.text("130,048")
				dom_status.append(" + 1,024 Shared")
			}
		}
		update_mem(0)
	}

	floppy_throb() {
		$("#bios-status").append('<span id="bios-status-floppy-throb"></span>')
		const dom_throb = $("#bios-status-floppy-throb")
		this.floppy_throb_interval = setInterval(() => {
			if (dom_throb.text() === '/')
				dom_throb.text('-')
			else if (dom_throb.text() === '-')
				dom_throb.text('\\')
			else if (dom_throb.text() === '\\')
				dom_throb.text('|')
			else
				dom_throb.text('/')
		}, 100)
	}

	floppy_throb_stop() {
		clearInterval(this.floppy_throb_interval)
	}

	clear_term() {
		$("#bios-status").text(null)
		$("#bios").hide()
		$("#bios-cleared").show()
	}

	graphical_boot() {
		$("#bios-cleared").hide()
		clearInterval(this.blink_interval);
		new Splash().run();
	}
}
