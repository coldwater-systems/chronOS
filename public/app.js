"use strict";
$(() => {
		$("#btn-start-game").click(e => {
			$("body").fullscreen();
			$("body").addClass("game-active")
			new Bios().run()
		})

		$("#btn-start-game-fast-boot").click(e => {
			$("body").fullscreen();
			$("body").addClass("game-active")
			new Desktop().run()
		})

		const query_string = new URLSearchParams(document.location.search.substring(1))
		const is_dev = query_string.get("dev")
		if (is_dev === "xyzzy")
			$("#btn-start-game-fast-boot").css("display", "block")
})
