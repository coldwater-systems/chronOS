$(() => {
	const query_string = new URLSearchParams(document.location.search.substring(1))
	const username = query_string.get("user")
	let directory = query_string.get("uri")

	const update_pwd = function() {
		if (directory.startsWith("\\"))
			directory = directory.substring(1)
		if (!directory.endsWith("\\"))
			directory += "\\"
		const path_components = directory.split("\\")
		let data_pwd = $("#fs-root")
		const dom_curdir = $("#current-dir")
		const dom_template = $("#file-template")

		$("#fs-address").val(directory.substring(0, directory.length - 1))

		for (let i = 0; i < path_components.length; i++) {
			if (path_components[i] !== "")
				data_pwd = data_pwd.find('div[data-name="' + path_components[i] + '"]')
			const acl_allow = data_pwd.data("acl-allow")
			const acl_sha = data_pwd.data("acl-sha")
			if (acl_allow !== undefined && acl_allow !== username) {
				dom_curdir.html('<div class="fs-curdir-invalid">You do not have access to this item. This folder is owned by ' + acl_allow + '</div>')
				return;
			}
			if (acl_sha !== undefined && acl_sha !== "") {
				new Dialog({
					attach: "body",
					id: "dlg-protected",
					window_title: "Password Proected Folder",
					title: "This folder is protected by a password",
					description: 'Password: <input type="password" id="dlg-protected-password">',
					handle_ok: d => {
						sha256sum($("#dlg-protected-password").val()).then(digest => {
							const hexdigest = Array.prototype.map.call(new Uint8Array(digest), x => ('00' + x.toString(16)).slice(-2)).join('');
							if (hexdigest === acl_sha) {
								data_pwd.data("acl-sha", "")
								update_pwd()
								d.close()
								return;
							}
							else {
								new Dialog({
									attach: "body",
									id: "dlg-protected-badpw",
									window_title: "Password Protected Folder",
									title: "Incorrect Code",
									description: 'Please try again',
									handle_esc: d => {
										d.close()
									},
									handle_ok: d => {
										d.close()
									}
								})
							}
						})
					},
					handle_esc: d => {
						d.close()
					},
					handle_cancel: d => {
						d.close()
					}
				})
				dom_curdir.html('<div class="fs-curdir-invalid">You do not have access to this item. You must type in the correct code.</div>')
				return;
			}
		}

		dom_curdir.empty()
		if (data_pwd.length === 0)
			dom_curdir.html('<div class="fs-curdir-invalid">This folder doesn\'t exist.</div>')
		else {
			data_pwd.children().each(function(i) {
				const ul = $(this)
				const name = ul.data("name")
				const type = ul.data("type")
				const source = ul.data("source")
				const new_icon = dom_template.clone()
				new_icon.attr("id", null)
				new_icon.addClass(type)
				if (!(type === "directory" || type === "trash"))
					new_icon.addClass("file")
				else
					new_icon.addClass("directory")
				if (name.endsWith(":"))
					new_icon.find(".name").text(name + "\\");
				else
					new_icon.find(".name").text(name);
				new_icon.data("source", source);
				dom_curdir.append(new_icon)

				$(".shortcut").off("dblclick")
				$(".shortcut.directory").dblclick(e => {
					subpath = $(e.target).parent(".shortcut").find(".name").text()
					if (!subpath.endsWith("\\"))
						subpath += "\\"
					directory += subpath
					update_pwd()
				})

				$(".shortcut.file").dblclick(e => {
					subpath = $(e.target).parent(".shortcut").find(".name").text()
					path = directory + subpath
					window.top.postMessage("fs:" + path.replace(":", ""), "*")
				})
			})
		}
	}
	update_pwd()

	$("body").click(e => {
		$(".shortcut").removeClass("active")
		$("#startmenu").hide()
	})

	$(".shortcut").click(e => {
		$(".shortcut").removeClass("active")
		$(e.target).parent(".shortcut").addClass("active")
		e.stopPropagation()
	})

	$("#btn-fs-up").click(e => {
		const path_components = directory.split("\\").slice(0, -2)
		directory = path_components.join("\\")
		update_pwd()
	})
})
