
class Dialog {
	constructor(opts) {
		this.id = opts.id
		const dom_new_dialog = $("#template-ok-cancel-dialog").clone()
		let attach = opts.attach
		if (attach === undefined)
			attach = "#desktop"
		dom_new_dialog.attr("id", opts.id)
		dom_new_dialog.addClass("dynamic")
		$(attach).append(dom_new_dialog)
		dom_new_dialog.draggable({ handle: ".titlebar", cancel: ".titlebar .button", containment: "#desktop", stack: ".window"})
		dom_new_dialog.find(".titlebar .title").text(opts.window_title)
		dom_new_dialog.find(".content .title").text(opts.title)
		dom_new_dialog.find(".content .description").html(opts.description)
		if (opts.handle_ok === undefined)
			dom_new_dialog.find(".btn-ok").hide()
		else
			dom_new_dialog.find(".btn-ok").click(() => opts.handle_ok(this))
		if (opts.handle_cancel === undefined)
			dom_new_dialog.find(".btn-cancel").hide()
		else
			dom_new_dialog.find(".btn-cancel").click(() => opts.handle_cancel(this))
		dom_new_dialog.find(".window-close").click(() => opts.handle_esc(this))
		dom_new_dialog.show()
	}

	close() {
		$("#" + this.id).remove()
	}
}

class Document {
	constructor(opts) {
		this.id = opts.id
		const dom_new_document = $("#template-document-window").clone()
		let attach = opts.attach
		if (attach === undefined)
			attach = "#desktop"
		dom_new_document.attr("id", opts.id)
		dom_new_document.addClass("dynamic")
		if (opts.add_class !== undefined)
			dom_new_document.addClass(opts.add_class)
		$(attach).append(dom_new_document)
		dom_new_document.draggable({ handle: ".titlebar", cancel: ".titlebar .button", containment: "#desktop", stack: ".window"})
		dom_new_document.find(".titlebar .title").text(opts.title)
		dom_new_document.find(".content").attr("src", opts.url)
		dom_new_document.find(".window-close").click(() => this.close())

		dom_new_document.css("border-color", opts.theme_color[0] + " " + opts.theme_color[2] + " " + opts.theme_color[2] + " " + opts.theme_color[0])
		dom_new_document.css("background", "linear-gradient(90deg, " + opts.theme_color[0] + " 0%, " + opts.theme_color[2] + " 100%)");
		dom_new_document.find(".titlebar").css("background-color", opts.theme_color[1])

		dom_new_document.show()
	}

	close() {
		$("#" + this.id).remove()
	}
}

function sha256sum(message) {
	//Source: https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest
	const encoder = new TextEncoder();
  const data = encoder.encode(message);
  return window.crypto.subtle.digest('SHA-256', data);
}
