"use strict";

class Splash {
	constructor() {
		$("#splash").show()
		this.progress = 0
	}

	run() {
		const dom_progress = $("#splash .progressbar > *")
		const interval = setInterval(() => {
			if (this.progress > 1) {
				clearInterval(interval);
				this.progress = 1;
				this.swap_to_desktop()
			}
			dom_progress.css("width", this.progress * 100 + "%")
			this.progress += Math.max(0, random(-0.03, 0.01) * random(0, 20));
		}, 20);
	}

	swap_to_desktop() {
		setTimeout(() => {
			$("#splash").hide()
			new Desktop().run()
		}, 1500)
	}
}

function random(min, max) {
	return Math.random() * (max - min) + min
}
