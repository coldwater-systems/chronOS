"use strict";

/** it's hashed. Nice try though. */
const password = "c079b1a60420f773db62d8b3e64b8a6019f0568eabe00e58b662a8068bd4ae97"
const hunter_2 = "bc3c1f589b8cd2713c671cd6686e05fb80260fa8ee0e2b3b2034997b235f3fc9"

class Desktop {
	constructor() {
		this.document_counter = 0 //used to generate unique IDs for dynamic DOM junk
		this.wall_time = 852076800 //Jan 1st 1997 @ midnight
		this.wall_interval = setInterval(() => {
			this.wall_time++
			this.update_time()
		}, 1000);

		$("body").removeClass("hide-cursor")
		$("#desktop").show()
		$(".window").draggable({ handle: ".titlebar", cancel: ".titlebar .button", containment: "#desktop", stack: ".window"})
		$("#login-username").change(e => {
			if ($(e.target).val() === "Guest")
				$("#login-password-row > *").hide()
			else
				$("#login-password-row > *").show()
		})

		$("#btn-shutdown").click(() => this.shutdown())
		$("#btn-restart").click(() => this.restart())

		const login_handler = e => {
			if ($("#login-username").val() === "Guest") {
				this.hide_login()
				setTimeout(() => {
					this.logged_in($("#login-username").val())
				}, 1000)
			}
			else
				setTimeout(() => this.check_password(), 1000)
			e.preventDefault()
		}
		$("#btn-login").click(login_handler)
		$("#login-password-row").submit(login_handler)

		$("#login-window .window-close").click(this.hide_login)

		//Logged in handling follows//

		$("#desktop").click(e => {
			$(".shortcut").removeClass("active")
			$("#startmenu").hide()
		})

		$(".shortcut").click(e => {
			$(".shortcut").removeClass("active")
			$(e.target).parent(".shortcut").addClass("active")
			e.stopPropagation()
		})

		$("#btn-startmenu").click(e => {
			$("#startmenu").toggle()
			e.stopPropagation()
		})

		$("#startmenu-logoff").click(e => {
			$("#desktop .dynamic").remove();
			$("#dialog-set-time").hide();
			$("#logged-in").hide();
			this.run();
		})

		$("#startmenu-shutdown").click(e => {
			new Dialog({
				id: "startmenu-shutdown-warn",
				window_title: "Are you sure?",
				title: "Are you sure?",
				description: "you want to shutdown the system?",
				handle_ok: d => {
					d.close()
					this.shutdown()
				},
				handle_esc: d => {
					d.close()
				},
				handle_cancel: d => {
					d.close()
				}
			})
		})

		$("#startmenu-restart").click(e => {
			new Dialog({
				id: "startmenu-restart-warn",
				window_title: "Are you sure?",
				title: "Are you sure?",
				description: "you want to restart the system?",
				handle_ok: d => {
					d.close()
					this.restart()
				},
				handle_esc: d => {
					d.close()
				},
				handle_cancel: d => {
					d.close()
				}
			})
		})

		$("#shortcut-trash").dblclick(() => new Document({
			id: "document-" + (this.document_counter++),
			title: "ChronOS Navigator",
			url: "apps/navigator.html?user=" + this.username + "&uri=Trash",
			theme_color: ["#FFF1A6", "#CCB954", "#AA9C50"]
		}))

		$("#shortcut-navigator").dblclick(() => new Document({
			id: "document-" + (this.document_counter++),
			title: "ChronOS Navigator",
			url: "apps/navigator.html?user=" + this.username + "&uri=X:\\",
			theme_color: ["#FFF1A6", "#CCB954", "#AA9C50"]
		}))

		window.onmessage = e => {
			if (e.data.startsWith("fs:")) {
				const uri = e.data.substring(3)
				if (uri.endsWith("note.txx")) {
					if (this.wall_time < 946684740) {
						new Dialog({
							id: "dlg-timelock",
							window_title: "Certificate Error",
							title: "There is a certificate lock on this file that expires @ 12/31/99 at 11:59PM",
							description: "Please wait until then...",
							handle_esc: d => {
								d.close()
							},
							handle_cancel: d => {
								d.close()
							}
						})
					}
					else {
						new Audio("res/audio/welcome_to_chronos.ogg").play()
						new Document({
							id: "document-" + (this.document_counter++),
							add_class: "trapeze",
							title: uri.split("\\").slice(-1)[0] + " - Trapeze Document Viewer", //XXX: underhanded way of doing .last
							url: "fs/" + uri + ".html",
							theme_color: ["#A5E4BB", "#5BA66C", "#2D6239"]
						})
					}
				}
				else {
					new Document({
						id: "document-" + (this.document_counter++),
						add_class: "trapeze",
						title: uri.split("\\").slice(-1)[0] + " - Trapeze Document Viewer", //XXX: underhanded way of doing .last
						url: "fs/" + uri + ".html",
						theme_color: ["#A5E4BB", "#5BA66C", "#2D6239"]
					})
				}
			}
			else {
				console.log("no handler for URI: " + e.data)
			}
		}

		$("#system-clock-well").click(e => {
			$("#dialog-set-time").show();
		})

		$("#dialog-set-time .window-close").click(e => {
			$("#input-time").addClass("update")
			$("#input-date").addClass("update")
			$("#dialog-set-time").hide();
		})

		$("#dialog-set-time .btn-confirm").click(e => {
			$("#input-time").addClass("update")
			$("#input-date").addClass("update")
			this.wall_time = Math.floor(new Date($("#input-date").val() + "T" + $("#input-time").val() + "Z") / 1000);
			this.update_time();
			$("#dialog-set-time").hide();
		})

		const remove_update = e => {
			$("#input-time").removeClass("update")
			$("#input-date").removeClass("update")
		}
		$("#input-time").click(remove_update)
		$("#input-date").click(remove_update)
		$("#input-time").keydown(remove_update)
		$("#input-date").keydown(remove_update)
	}

	run() {
		setTimeout(() => {
			$("#login-username").val("JBanner")
			$("#login-password-row > *").show()
			$("#login-password").val(null)
			$("#login-window").show()
			$("#login-password").focus()
		}, 1000);
	}


	check_password() {
		// A real password hash should be used in a real system. Do as I say not as I do
		sha256sum($("#login-password").val()).then(digest => {
			const hexdigest = Array.prototype.map.call(new Uint8Array(digest), x => ('00' + x.toString(16)).slice(-2)).join('');
			if (hexdigest === password || hexdigest === hunter_2) {
				this.hide_login()
				this.logged_in($("#login-username").val())
			}
			else {
				new Dialog({
					id: "login-error",
					window_title: "SLS Login",
					title: "Authentication Failure",
					description: "The system cannot log you on because your username or password is incorrect. Please check your spelling and try again. For help, please log in as a guest and use the password recovery tool or call +1 (719) 266-2837 for support.",
					handle_ok: d => {
						d.close()
						$("#login-password").select()
						$("#login-password").focus()
					},
					handle_esc: d => {
						d.close()
						$("#login-password").select()
						$("#login-password").focus()
					},
					handle_cancel: d => {
						d.close()
						this.hide_login()
					}
				}) //end Dialog
			} //end if
		}) //end sha256sum
	}

	hide_login() {
		$("#login-window").hide();
	}

	hide_desktop() {
		$("#desktop").hide();
	}

	logged_in(username) {
		this.username = username
		$("#startmenu-username").text(username)
		$("#logged-in").show()
		new Audio("res/audio/ChronOSStartup.ogg").play()
		if (username === "Guest") {
			setTimeout(() => {
				new Dialog({
					id: "dlg-security-warning",
					window_title: "ChronOS User System Security Updater Utility",
					title: "Your system is out of date",
					description: 'Please update to latest secure version. The update fixes a bug in the logon system where any account can access unencrypted passwords in the "passwords.txx" file.',
					handle_ok: d => {
						d.close()
					},
					handle_esc: d => {
						d.close()
					}
				})
			}, 5000)
		}
		else {
			setTimeout(() => {
				new Dialog({
					id: "dlg-security-warning",
					window_title: "ChronOS Power Manager",
					title: "System Failure Warning",
					description: 'Due to power failure, the ChronOS Chronograph may be set incorrectly. Please reset the clock to correct time.',
					handle_ok: d => {
						d.close()
					},
					handle_esc: d => {
						d.close()
					}
				})
			}, 5000)
		}
	}

	update_time() {
		const date = new Date(this.wall_time * 1000)
		const well_format = new Intl.DateTimeFormat('en-US', {hour12: true, hour: '2-digit', minute: '2-digit', timeZone: "UTC"})
		const settime_format = new Intl.DateTimeFormat('en-US', {hour12: false, hour: '2-digit', minute: '2-digit', second: '2-digit', timeZone: "UTC"})
		$("#system-clock").text(well_format.format(date))
		$("#input-time.update").val(settime_format.format(date))
		$("#input-date.update").val(date.toISOString().split("T")[0])

		if (date.getUTCFullYear() >= 2000)
			setTimeout(() => this.panic(), 1000);
	}

	shutdown() {
		new Audio("res/audio/ChronOSShutdown.ogg").play()
		$("#logged-in").hide()
		clearInterval(this.wall_interval)
		setTimeout(() => this.hide_login(), 1000)
		setTimeout(() => this.hide_desktop(), 2000)
		setTimeout(() => {
			window.location.reload(true)
		}, 4000)
	}

	restart() {
		new Audio("res/audio/ChronOSShutdown.ogg").play()
		$("#logged-in").hide()
		clearInterval(this.wall_interval)
		setTimeout(() => this.hide_login(), 1000)
		setTimeout(() => this.hide_desktop(), 2000)
		setTimeout(() => {
			$("*").off();
			new Bios().run()
		}, 4000)
	}

	panic() {
		$("#logged-in").hide()
		this.hide_login()
		this.hide_desktop()
		clearInterval(this.wall_interval)
		$("#bsod").show()
		setTimeout(() => this.shutdown(), 7500)
	}
}
