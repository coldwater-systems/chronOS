# ChronOS

The year is 2000, and you have recently purchased a desktop computer at a surplus auction from a failed company. You could just wipe the drive and start anew, but that wouldn’t be fun would it? You should probably find out who owned this computer and why this company went under. It won’t be easy though, information hides behind codes and encryption (though it may not be that strong), and it’s your job to gather the clues you need to find more information. You may want a pen and paper to write down important notes for your quest.

[Play the game](https://a.lexg.dev/ChronOS)
